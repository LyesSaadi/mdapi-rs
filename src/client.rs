use crate::DEFAULT_INSTANCE;
use crate::error::Result;

use reqwest::{Url, blocking::{self, Response}};

pub struct Client {
    pub endpoint: Url,
    client: blocking::Client,
}

impl Client {
    pub fn new() -> Result<Self> {
        Ok(Self {
            endpoint: Url::parse(DEFAULT_INSTANCE).unwrap(),
            client: blocking::Client::new()
        })
    }

    pub fn get(&self, path: &str) -> reqwest::Result<Response> {
        let mut url = self.endpoint.clone();
        url.set_path(path);

        self.client.get(url).send()
    }
}