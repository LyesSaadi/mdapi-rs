//! `mdapi-rs` is Rusty client to the Fedora
//! [`mdapi`](https://mdapi.fedoraproject.org/).

const DEFAULT_BRANCH: &str = "koji";
const DEFAULT_INSTANCE: &str = "https://mdapi.fedoraproject.org";

pub mod branch;
pub mod client;
pub mod error;
pub mod files;
pub mod package;
pub mod request;

// Re-exports
pub use branch::{Branches, Branch, BranchRepository};
pub use client::Client;
pub use package::Package;
pub use request::Request;