//! Module containing all errors which might be returned by `mdapi-rs`.

use std::error;
use std::fmt::Display;

use reqwest::StatusCode;
use reqwest::Error as ReqwestError;
use serde_json::Error as JsonError;

pub type Result<T> = std::result::Result<T, Error>;

/// An internal error with the `mdapi` server which is being used.
///
/// This is returned when a request to mdapi returns 500.
#[derive(Debug)]
pub struct MdapiError;

impl error::Error for MdapiError {}

impl Display for MdapiError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "An internal mdapi error occured")
    }
}

/// A non-existing branch or a bad request was made to mdapi.
///
/// This is returned when a request to mdapi returns 400.
#[derive(Debug)]
pub struct BranchError {
    pub name: String
}

impl error::Error for BranchError {}

impl Display for BranchError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "The \"{}\" branch does not exist", self.name)
    }
}

/// A non-existing package was requested to `mdapi`.
///
/// This is returned when a request to mdapi returns 404.
#[derive(Debug)]
pub struct PackageError {
    pub name: String,
    pub branch: String
}

impl error::Error for PackageError {}

impl Display for PackageError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "No \"{}\" package exists for the \"{}\" branch", self.name,
            self.branch)
    }
}

/// An error while reaching `mdapi` which `mdapi-rs` was not able to interpret.
#[derive(Debug)]
pub struct UnknownError {
    pub msg: String
}

impl error::Error for UnknownError {}

impl Display for UnknownError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.msg)
    }
}

/// An enum representing all Errors which may be returned by `mdapi`.
#[derive(Debug)]
pub enum Error {
    /// When a [`MdapiError`](crate::error::MdapiError) is returned.
    Mdapi(MdapiError),

    /// When a [`BranchError`](crate::error::BranchError) is returned.
    Branch(BranchError),

    /// When a [`PackageError`](crate::error::PackageError) is returned.
    Package(PackageError),

    /// When an [`UnknownError`](crate::error::UnknownError) is returned.
    Unknown(UnknownError),

    /// When an [`Error`](reqwest::Error) while trying to reach `mdapi` by
    /// `reqwest` is returned.
    Reqwest(ReqwestError),

    /// When an [`Error`](serde_json::Error) while parsing and serializing JSON
    /// by `serde_json` is returned.
    Json(JsonError)
}

impl Error {
    /// Parse the status code to return the appropriate
    /// [`Error`](crate::error::Error).
    pub fn from_status_code(status: StatusCode, branch: Option<String>, package: Option<String>) -> Result<()> {
        if !status.is_success() {
            match (status.as_u16(), branch, package) {
                (400, Some(branch), _) => Err(Error::Branch(BranchError {
                    name: branch
                })),
                (404, Some(branch), Some(package)) => Err(Error::Package(PackageError {
                    name: package,
                    branch
                })),
                (500, _, _) => Err(Error::Mdapi(MdapiError)),
                _ => Err(Error::Unknown(UnknownError {
                    msg: format!("Query returned unexpected {} status code", status.as_str())
                }))
            }
        } else {
            Ok(())
        }
    }
}

impl error::Error for Error {}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::Mdapi(e) => e.fmt(f),
            Error::Branch(e) => e.fmt(f),
            Error::Package(e) => e.fmt(f),
            Error::Unknown(e) => e.fmt(f),
            Error::Reqwest(e) => e.fmt(f),
            Error::Json(e) => e.fmt(f)
        }
    }
}

impl From<ReqwestError> for Error {
    fn from(e: ReqwestError) -> Self {
        Error::Reqwest(e)
    }
}

impl From<JsonError> for Error {
    fn from(e: JsonError) -> Self {
        Error::Json(e)
    }
}