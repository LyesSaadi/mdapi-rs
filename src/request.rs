use serde::Deserialize;

use crate::{Client, Package, Branch, DEFAULT_BRANCH, error::{Error, UnknownError, Result}};

#[derive(Debug, Deserialize)]
pub struct Request {
    pub name: String,
    pub epoch: Option<String>,
    pub version: Option<String>,
    pub release: Option<String>,
    pub flags: Option<Flag>
}

impl Request {
    fn request(client: Client, name: String, branch: Option<Branch>, request_type: String) -> Result<Vec<Package>> {
        let branch = match branch {
            Some(b) => b.name,
            None => String::from(DEFAULT_BRANCH)
        };

        let url = format!("{}/{}/{}", branch, request_type, name);

        let response = client.get(&url)?;

        // For some reason, both errors are indistinguishable in `mdapi`.
        if response.status().as_u16() == 400 {
            return Err(Error::Unknown(UnknownError {
                msg: format!("Either the branch \"{}\" do not exist or there is no \"{}\" {} in the repositories", branch, name, request_type)
            }));
        }

        Error::from_status_code(response.status(), Some(branch), None)?;

        Ok(serde_json::from_str(&response.text()?)?)
    }

    /// Query requires for a certain request.
    pub fn requires(name: String, branch: Option<Branch>) -> Result<Vec<Package>> {
        let client = Client::new()?;

        Self::requires_with_client(client, name, branch)
    }

    /// Query requires for a certain request with a custom client.
    pub fn requires_with_client(client: Client, name: String, branch: Option<Branch>) -> Result<Vec<Package>> {
        Self::request(client, name, branch, String::from("requires"))
    }

    /// Query provides for a certain request.
    pub fn provides(name: String, branch: Option<Branch>) -> Result<Vec<Package>> {
        let client = Client::new()?;

        Self::provides_with_client(client, name, branch)
    }

    /// Query provides for a certain request with a custom client.
    pub fn provides_with_client(client: Client, name: String, branch: Option<Branch>) -> Result<Vec<Package>> {
        Self::request(client, name, branch, String::from("provides"))
    }

    /// Query obsoletes for a certain request.
    pub fn obsoletes(name: String, branch: Option<Branch>) -> Result<Vec<Package>> {
        let client = Client::new()?;

        Self::obsoletes_with_client(client, name, branch)
    }

    /// Query obsoletes for a certain request with a custom client.
    pub fn obsoletes_with_client(client: Client, name: String, branch: Option<Branch>) -> Result<Vec<Package>> {
        Self::request(client, name, branch, String::from("obsoletes"))
    }

    /// Query conflicts for a certain request.
    pub fn conflicts(name: String, branch: Option<Branch>) -> Result<Vec<Package>> {
        let client = Client::new()?;

        Self::conflicts_with_client(client, name, branch)
    }

    /// Query conflicts for a certain request with a custom client.
    pub fn conflicts_with_client(client: Client, name: String, branch: Option<Branch>) -> Result<Vec<Package>> {
        Self::request(client, name, branch, String::from("conflicts"))
    }

    /// Query enhances for a certain request.
    pub fn enhances(name: String, branch: Option<Branch>) -> Result<Vec<Package>> {
        let client = Client::new()?;

        Self::enhances_with_client(client, name, branch)
    }

    /// Query enhances for a certain request with a custom client.
    pub fn enhances_with_client(client: Client, name: String, branch: Option<Branch>) -> Result<Vec<Package>> {
        Self::request(client, name, branch, String::from("enhances"))
    }

    /// Query recommends for a certain request.
    pub fn recommends(name: String, branch: Option<Branch>) -> Result<Vec<Package>> {
        let client = Client::new()?;

        Self::recommends_with_client(client, name, branch)
    }

    /// Query recommends for a certain request with a custom client.
    pub fn recommends_with_client(client: Client, name: String, branch: Option<Branch>) -> Result<Vec<Package>> {
        Self::request(client, name, branch, String::from("recommends"))
    }

    /// Query suggests for a certain request.
    pub fn suggests(name: String, branch: Option<Branch>) -> Result<Vec<Package>> {
        let client = Client::new()?;

        Self::suggests_with_client(client, name, branch)
    }

    /// Query suggests for a certain request with a custom client.
    pub fn suggests_with_client(client: Client, name: String, branch: Option<Branch>) -> Result<Vec<Package>> {
        Self::request(client, name, branch, String::from("suggests"))
    }

    /// Query supplements for a certain request.
    pub fn supplements(name: String, branch: Option<Branch>) -> Result<Vec<Package>> {
        let client = Client::new()?;

        Self::supplements_with_client(client, name, branch)
    }

    /// Query supplements for a certain request with a custom client.
    pub fn supplements_with_client(client: Client, name: String, branch: Option<Branch>) -> Result<Vec<Package>> {
        Self::request(client, name, branch, String::from("supplements"))
    }
}

#[allow(clippy::upper_case_acronyms)]
#[derive(Debug, Deserialize)]
pub enum Flag {
    EQ,
    GT,
    GE,
    LT,
    LE,
    NE
}