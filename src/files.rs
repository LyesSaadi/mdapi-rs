use std::{cell::RefCell, convert::TryFrom, ffi::OsString, ops::{Deref, DerefMut}, rc::Rc};

use serde::Deserialize;

use crate::error;

#[derive(Debug, Deserialize)]
pub(crate) struct PrivateFiles {
    files: Vec<PrivateFileList>
}

#[derive(Debug, Deserialize)]
pub(crate) struct PrivateFileList {
    dirname: String,
    filenames: String,
    filetypes: String
}

pub(crate) enum PrivateFileType {
    File,
    Directory
}

#[derive(Debug)]
pub struct FileTree {
    pub children: Vec<Rc<RefCell<Node>>>
}

impl FileTree {
    pub fn contains(&self, name: OsString) -> Option<Rc<RefCell<Node>>> {
        for node in &self.children {
            match node.borrow().deref() {
                Node::File(f) => {
                    if f.name == name {
                        return Some(node.clone());
                    }
                },
                Node::Directory(d) => {
                    if d.name == name {
                        return Some(node.clone());
                    }
                }
            }
        }

        None
    }

    fn create(&mut self, path: &[&str], file: &str, filetype: PrivateFileType) {
        if path.len() > 1 {
            let node = match self.contains(path[0].into()) {
                Some(n) => n,
                None => {
                    let node = Rc::new(RefCell::new(Node::Directory(Directory {
                        children: Vec::new(),
                        name: path[0].into()
                    })));

                    self.children.push(node.clone());

                    node
                }
            };

            let node = Node::traverse(node, &path[1..]);

            let mut borrowed_node = node.borrow_mut();

            if let Node::Directory(d) = borrowed_node.deref_mut() {
                let new = match filetype {
                    PrivateFileType::Directory => {
                        Rc::new(RefCell::new(Node::Directory(Directory {
                            children: Vec::new(),
                            name: file.into()
                        })))
                    },
                    PrivateFileType::File => {
                        Rc::new(RefCell::new(Node::File(File {
                            name: file.into()
                        })))
                    }
                };

                d.children.push(new);
            }
        }
    }
}

impl TryFrom<PrivateFiles> for FileTree {
    type Error = crate::error::Error;

    fn try_from(p: PrivateFiles) -> Result<Self, Self::Error> {
        let mut tree = FileTree {
            children: Vec::new()
        };

        let files = p.files;

        for list in files {
            let path: Vec<&str> = list.dirname.split('/').filter(|s| !s.is_empty()).collect();

            for file in list.filenames.split('/').zip(list.filetypes.as_bytes()) {
                let filetype = match *file.1 as char {
                    'f' => PrivateFileType::File,
                    'd' => PrivateFileType::Directory,
                    'g' => continue,
                    c => {
                        return Err(error::Error::Unknown(
                            error::UnknownError {
                                msg: format!("Unknown file type {}", c)
                            }
                        ));
                    }
                };

                tree.create(&path, file.0, filetype);
            }
        }

        Ok(tree)
    }
}

#[derive(Debug)]
pub enum Node {
    File(File),
    Directory(Directory)
}

impl Node {
    pub fn traverse(node: Rc<RefCell<Node>>, path: &[&str]) -> Rc<RefCell<Node>> {
        if path.is_empty() {
            return node;
        }

        match node.borrow_mut().deref_mut() {
            Node::File(_) => node.clone(),
            Node::Directory(d) => {
                let cnode = match d.contains(path[0].into()) {
                    Some(n) => n,
                    None => {
                        Rc::new(RefCell::new(Node::Directory(Directory {
                            children: Vec::new(),
                            name: path[0].into()
                        })))
                    }
                };

                d.children.push(cnode.clone());

                Self::traverse(cnode, &path[1..])
            }
        }
    }
}

#[derive(Debug)]
pub struct File {
    pub name: OsString
}

#[derive(Debug)]
pub struct Directory {
    pub children: Vec<Rc<RefCell<Node>>>,
    pub name: OsString
}

impl Directory {
    pub fn contains(&self, name: OsString) -> Option<Rc<RefCell<Node>>> {
        for node in &self.children {
            match node.borrow().deref() {
                Node::File(f) => {
                    if f.name == name {
                        return Some(node.clone());
                    }
                },
                Node::Directory(d) => {
                    if d.name == name {
                        return Some(node.clone());
                    }
                }
            }
        }

        None
    }
}