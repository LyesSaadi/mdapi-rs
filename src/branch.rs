//! Module containing structures for mdapi branches.

use crate::error::{Error, Result};
use crate::client::Client;

/// Contains all the available branches from mdapi.
#[derive(Debug)]
pub struct Branches {
    pub branches: Vec<Branch>
}

impl Branches {
    /// Fetches new branches.
    pub fn new() -> Result<Self> {
        let client = Client::new()?;

        Self::new_with_client(client)
    }

    /// Fetches new branches with a custom client.
    pub fn new_with_client(client: Client) -> Result<Self> {
        let result = client.get("branches")?;

        Error::from_status_code(result.status(), None, None)?;

        let result = result.text()?;

        let branches: Vec<&str> = serde_json::from_str(&result)?;

        let branches: Vec<Branch> = branches.iter().filter_map(Branch::parse).collect();

        Ok(Self { branches })
    }
}

/// Represents a mdapi branch.
#[derive(Debug, PartialEq, Eq)]
pub struct Branch {
    /// Which repo is the branch representing.
    pub repo: BranchRepository,

    /// Which version of the repository is it.
    ///
    /// ## Note
    /// This is None in cases like `rawhide` or `koji` where it doesn't make any
    /// sense.
    pub version: Option<u8>,

    /// The mdapi name of the branch.
    pub name: String
}

impl Branch {
    /// Parses the branch name into a usable Branch structure.
    ///
    /// # Planning for the Future™
    /// Branches that break current naming conventions will be simply ignored.
    ///
    /// # Notes
    ///
    /// ## Source branches
    /// We purposefully ignore source branches since one exists for each binary
    /// branch and that they don't interact well with the `srcpkg` API.
    ///
    /// ## Legacy branches
    /// At the time of writing, `dist-6E` and `dist-6E-epel` branches exists.
    /// These should represent EL6 branches, which are EOL. Because they don't
    /// respect the new naming convention, they are not of critical impotance
    /// and they will be soon phased out, they will be purposefully ignored.
    pub fn parse(branch: &&str) -> Option<Branch> {
        let branch = *branch;

        // -- Handling special cases --

        // If the branch "koji" exists
        if branch == "koji" {
            return Some(Branch {
                repo: BranchRepository::Koji,
                version: None,
                name: String::from("koji")
            });
        }

        // If the branch "rawhide" exists
        if branch == "rawhide" {
            return Some(Branch {
                repo: BranchRepository::Fedora,
                version: None,
                name: String::from("rawhide")
            });
        }

        // If it is a Fedora branch
        if branch.starts_with('f') {
            let version = match branch.strip_prefix('f').unwrap().parse() {
                Ok(v) => {
                    v
                },
                Err(_) => {
                    return None;
                }
            };

            return Some(Branch {
                repo: BranchRepository::Fedora,
                version: Some(version),
                name: String::from(branch)
            });
        }

        // If it is an Epel branch
        if branch.starts_with("epel") {
            let version = match branch.strip_prefix("epel").unwrap().parse() {
                Ok(v) => {
                    v
                },
                Err(_) => {
                    return None;
                }
            };

            return Some(Branch {
                repo: BranchRepository::Epel,
                version: Some(version),
                name: String::from(branch)
            });
        }

        None
    }
}

#[derive(Debug, PartialEq, Eq)]
pub enum BranchRepository {
    Fedora,
    Epel,
    Koji
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn parsing_branch() {
        assert_eq!(Branch::parse(&"koji"), Some(Branch {
            repo: BranchRepository::Koji,
            version: None,
            name: String::from("koji")
        }));

        assert_eq!(Branch::parse(&"rawhide"), Some(Branch {
            repo: BranchRepository::Fedora,
            version: None,
            name: String::from("rawhide")
        }));
 
        assert_eq!(Branch::parse(&"f34"), Some(Branch {
            repo: BranchRepository::Fedora,
            version: Some(34),
            name: String::from("f34")
        }));
 
        assert_eq!(Branch::parse(&"epel8"), Some(Branch {
            repo: BranchRepository::Epel,
            version: Some(8),
            name: String::from("epel8")
        }));
    }

    #[test]
    fn fetching_branches() {
        let branches = Branches::new().unwrap();

        assert!(branches.branches.contains(&Branch {
            repo: BranchRepository::Fedora,
            version: None,
            name: String::from("rawhide")
        }));
    }
}