use std::convert::TryInto;

use crate::DEFAULT_BRANCH;
use crate::{Request, Branch, Client};
use crate::error::{Result, Error};
use crate::files::*;

use chrono::{DateTime, Utc};
use serde::Deserialize;

/// Represents a package in `mdapi`.
#[derive(Debug, Deserialize)]
pub struct Package {
    pub arch: String,
    pub epoch: String,
    pub version: String,
    pub release: String,
    pub summary: String,
    pub description: String,
    pub basename: String,
    pub url: String,
    #[serde(rename = "co-packages")]
    pub co_packages: Vec<String>,
    pub repo: Repository,

    pub conflicts: Vec<Request>,
    pub obsoletes: Vec<Request>,
    pub provides: Vec<Request>,
    pub requires: Vec<Request>,
    pub enhances: Vec<Request>,
    pub recommends: Vec<Request>,
    pub suggests: Vec<Request>,
    pub supplements: Vec<Request>,

    #[serde(default)]
    pub is_source: bool
}

impl Package {
    /// Fetches a package.
    pub fn new(name: String, is_source: bool, branch: Option<Branch>) -> Result<Self> {
        let client = Client::new()?;

        Self::new_with_client(client, name, is_source, branch)
    }

    /// Fetches a package with a custom client.
    pub fn new_with_client(client: Client, name: String, is_source: bool, branch: Option<Branch>)
        -> Result<Self> {
        let branch = match branch {
            Some(b) => b.name,
            None => String::from(DEFAULT_BRANCH)
        };

        // If we want to get a source package, we use the srcpkg api.
        let api;
        if is_source {
            api = "srcpkg";
        } else {
            api = "pkg";
        }

        let url = format!("{}/{}/{}", branch, api, name);
        let response = client.get(&url)?;

        Error::from_status_code(response.status(), Some(branch), Some(name))?;

        let mut package: Package = serde_json::from_str(&response.text()?)?;

        package.is_source = is_source;

        Ok(package)
    }

    /// Fetches changelog information about a package.
    pub fn changelog(&self, branch: Option<Branch>) -> Result<Changelog> {
        let client = Client::new()?;

        self.changelog_with_client(client, branch)
    }

    /// Fetches changelog information about a package with a custom client.
    pub fn changelog_with_client(&self, client: Client, branch: Option<Branch>) -> Result<Changelog> {
        let branch = match branch {
            Some(b) => b.name,
            None => String::from(DEFAULT_BRANCH)
        };

        let url = format!("{}/changelog/{}", branch, self.basename);
        let response = client.get(&url)?;

        Error::from_status_code(response.status(), Some(branch), Some(self.basename.clone()))?;

        Ok(serde_json::from_str(&response.text()?)?)
    }

    /// Fetches the file list.
    ///
    /// # Bad API
    /// The [`FileTree`](crate::files::FileTree) API is bad a quite a hack. It
    /// is only temporary™ and will be improved in the future.
    ///
    /// It is still better though than the raw data returned by `mdapi`.
    pub fn files(&self, branch: Option<Branch>) -> Result<FileTree> {
        let client = Client::new()?;

        self.files_with_client(client, branch)
    }

    /// Fetches the file list with a custom client.
    ///
    /// # Bad API
    /// The [`FileTree`](crate::files::FileTree) API is bad a quite a hack. It
    /// is only temporary™ and will be improved in the future.
    ///
    /// It is still better though than the raw data returned by `mdapi`.
    pub fn files_with_client(&self, client: Client, branch: Option<Branch>) -> Result<FileTree> {
        let branch = match branch {
            Some(b) => b.name,
            None => String::from(DEFAULT_BRANCH)
        };

        let url = format!("{}/files/{}", branch, self.basename);
        let response = client.get(&url)?;

        Error::from_status_code(response.status(), Some(branch), Some(self.basename.clone()))?;

        let files: PrivateFiles = serde_json::from_str(&response.text()?)?;

        files.try_into()
    }
}

/// Represents a Changelog object
#[derive(Debug, Deserialize)]
pub struct Changelog {
    pub author: String,
    pub changelog: String,
    pub date: DateTime<Utc>
}

/// Represents the repository from which the package is.
#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum Repository {
    Release,
    Updates,
    UpdatesTesting,
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn it_works() {
        let package = Package::new(String::from("kernel"), false, None).unwrap();

        assert_eq!(package.basename, String::from("kernel"));
    }
}